package helpers

import (
	"encoding/json"
	"fmt"
	"iframe/models"
	"iframe/services"
)

const baseDarkSkyWeatherURL string = "https://api.darksky.net/forecast/c3134975128e0a7ede026b4dabfe75d2/"

type WeatherHelper struct{}

func (helper WeatherHelper) GetWeatherByCity(long string, lat string) ( *models.Weather, error) {
	url := fmt.Sprintf(baseDarkSkyWeatherURL+"%v,%v?extend=hourly&units=si", lat, long)

	var weatherData models.Weather

	apiService := new(services.ApiService)

	weatherResponseBody, err := apiService.GetResponse(url)
	if err != nil {
		return nil, err
	}


	if err := json.NewDecoder(weatherResponseBody).Decode(&weatherData); err != nil {
		return nil, err
	}

	return &weatherData, nil

}

func (helper WeatherHelper) getWeatherIntensity(icon string) int {
	switch icon {
	case "clear-day":
		return 0
	case "clear-night":
		return 10
	case "partly-cloudy-day":
		return 20
	case "partly-cloudy-night":
		return 30
	case "cloudy":
		return 40
	case "wind":
		return 50
	case "fog":
		return 60
	case "rain":
		return 70
	case "sleet":
		return 80
	case "snow":
		return 90
	default:
		return 0
	}
}