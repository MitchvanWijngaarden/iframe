package helpers

import (
	"errors"
	"iframe/models"
)

type PlaceHelper struct{}

var places []models.ApiPlace

func (helper PlaceHelper) InitPlaces() {
	place1 := models.ApiPlace {
		Place: "Primark Enschede",
		PlaceID: "ChIJw4cTe3EUuEcRJMLJxR3Tcwg",
		Latitude: "52.011520",
		Longitude: "4.710463",
		Url: "https://www.google.com/maps?cid=609062499450995236",
	}

	place2 := models.ApiPlace {
		Place: "Snap Fitness Breda",
		PlaceID: "ChIJywOALYmfxkcRnuobwQnSZ8w",
		Latitude: "52.011520",
		Longitude: "4.710463",
		Url: "https://www.google.com/maps?cid=14728972045674867358",
	}

	place3 := models.ApiPlace {
		Place: "McDonald's Leiden Beestenmarkt",
		PlaceID: "ChIJoWJ5O-zGxUcRkEUL3BT3sWU",
		Latitude: "52.162857",
		Longitude: "4.485127",
		Url: "https://www.google.com/maps?cid=7327909737671640464",
	}

	place4 := models.ApiPlace {
		Place: "Best Snack Gouda",
		PlaceID: "ChIJJfb0yq7WxUcRgAUYlCIAuwI",
		Latitude: "52.017898",
		Longitude: "4.710456",
		Url: "https://www.google.com/maps?cid=196751157234238848",
	}

	place5 := models.ApiPlace {
		Place: "Vondelpark, Amsterdam",
		PlaceID: "ChIJz3y0xeIJxkcRNcogBVV41Gw",
		Latitude: "52.357982",
		Longitude: "4.868560",
		Url: "https://www.google.com/maps?cid=7842025157712464437",
	}

	place6 := models.ApiPlace {
		Place: "Rock café Lazarus",
		PlaceID: "ChIJKUfbbOzGxUcRlMzCId3TaSs",
		Latitude: "52.163710",
		Longitude: "4.485967",
		Url: "https://www.google.com/maps?cid=3128264362893167764",
	}

	place7 := models.ApiPlace {
		Place: "LuckyGym",
		PlaceID: "ChIJkRQ1017GxUcRsV074GJjVyk",
		Latitude: "52.145181",
		Longitude: "4.481185",
		Url: "https://www.google.com/maps?cid=2978958954848804273",
	}

	place8 := models.ApiPlace {
		Place: "ANWB shop Leiden",
		PlaceID: "ChIJoQTrq-7GxUcRBnWlVQlfBKc",
		Latitude: "52.164490",
		Longitude: "4.483993",
		Url: "https://www.google.com/maps?cid=12034848597937059078",
	}

	place9 := models.ApiPlace {
		Place: "PLUS Van Ee",
		PlaceID: "ChIJH4YfbQfUxUcRQLbKY97tiXc",
		Latitude: "52.005730",
		Longitude: "4.701821",
		Url: "https://www.google.com/maps?cid=8613677301735536192",
	}

	place10 := models.ApiPlace {
		Place: "Albert Heijn Gouda",
		PlaceID: "ChIJ4SJxdQLUxUcR0ne3K0nUBI4",
		Latitude: "52.005730",
		Longitude: "4.701821",
		Url: "https://www.google.com/maps?cid=10233537664023754706",
	}

	place11 := models.ApiPlace {
	   Place: "ANWB shop Leiden",
	   PlaceID: "ChIJoQTrq-7GxUcRBnWlVQlfBKc",
	   Latitude: "52.164490",
	   Longitude: "4.483993",
	   Url: "https://www.google.com/maps?cid=12034848597937059078",
	}

	place12 := models.ApiPlace {
	   Place: "De Bruine Boon",
	   PlaceID: "ChIJSUHkAezGxUcRxCJAyC1kCZQ",
	   Latitude: "52.164380",
	   Longitude: "4.484509",
	   Url: "https://www.google.com/maps?cid=10667167340199682756",
	}

	place13 := models.ApiPlace {
	   Place: "Grand Café Van Buuren",
	   PlaceID: "ChIJv2Odqu7GxUcRleJTnpJ6kRc",
	   Latitude: "52.164552",
	   Longitude: "4.484394",
	   Url: "https://www.google.com/maps?cid=1698273304635499157",
	}

	place14 := models.ApiPlace {
	   Place: "Stadscafé Van der Werff",
	   PlaceID: "ChIJDb6PHOzGxUcRbe1OOAtbaNQ",
	   Latitude: "52.163976",
	   Longitude: "4.485109",
	   Url: "https://www.google.com/maps?cid=15305583437366095213",
	}

	place15 := models.ApiPlace {
	   Place: "El Fierro",
	   PlaceID: "ChIJ-anDne7GxUcRZEznMq1Upek",
	   Latitude: "52.162910",
	   Longitude: "4.484410",
	   Url: "https://www.google.com/maps?cid=16835955884854561892",
	}

	place16 := models.ApiPlace {
	   Place: "Selera Anda",
	   PlaceID: "ChIJWajDJuzGxUcRGfPKozzA4pU",
	   Latitude: "52.162869",
	   Longitude: "4.484421",
	   Url: "https://www.google.com/maps?cid=10800406223066428185",
	}

	place17 := models.ApiPlace {
	   Place: "Chicken Way",
	   PlaceID: "ChIJGYYBJ-zGxUcRLMFpltetHVE",
	   Latitude: "52.162799",
	   Longitude: "4.484452",
	   Url: "https://www.google.com/maps?cid=5845019032849858860",
	}

	place18 := models.ApiPlace {
	   Place: "Studio de Veste",
	   PlaceID: "ChIJp7tIGezGxUcRdBvEJ87M3sM",
	   Latitude: "52.163465",
	   Longitude: "4.484693",
	   Url: "https://www.google.com/maps?cid=14113943468028205940",
	}

	place19 := models.ApiPlace {
	   Place: "Ichi Ban",
	   PlaceID: "ChIJY1w0OezGxUcR_ZVsciPJElo",
	   Latitude: "52.162877",
	   Longitude: "4.485478",
	   Url: "https://www.google.com/maps?cid=6490471167075063293",
	}

	place20 := models.ApiPlace {
	   Place: "The Duke of Oz",
	   PlaceID: "ChIJVUYAROzGxUcRz7d68CLkAXY",
	   Latitude: "52.162898",
	   Longitude: "4.486182",
	   Url: "https://www.google.com/maps?cid=8503328410166802383",
	}

	place21 := models.ApiPlace {
	   Place: "Sumatra House",
	   PlaceID: "ChIJVVVVVezGxUcRxW5mpBMetV8",
	   Latitude: "52.163112",
	   Longitude: "4.486277",
	   Url: "https://www.google.com/maps?cid=6896451474099171013",
	}

	place22 := models.ApiPlace {
	   Place: "Restaurant Hudson Leiden",
	   PlaceID: "ChIJIQ26a-zGxUcRinVaHc9Opqw",
	   Latitude: "52.163446",
	   Longitude: "4.486279",
	   Url: "https://www.google.com/maps?cid=12440717672115238282",
	}

	place23 := models.ApiPlace {
	   Place: "eazie Leiden Stationsweg",
	   PlaceID: "ChIJrdtDU-nGxUcREv8-BXXhhc4",
	   Latitude: "52.164691",
	   Longitude: "4.483904",
	   Url: "https://www.google.com/maps?cid=14881548436431109906",
	}

	place24 := models.ApiPlace {
	   Place: "Domino's Pizza Leiden - Stationsweg",
	   PlaceID: "ChIJGVpH_-vGxUcRhWrCfbQ1ZX0",
	   Latitude: "52.164692",
	   Longitude: "4.484390",
	   Url: "https://www.google.com/maps?cid=9035687276709046917",
	}

	place25 := models.ApiPlace {
	   Place: "Yuniku Leiden",
	   PlaceID: "ChIJA4l0VenGxUcRC2DzEcaVLLI",
	   Latitude: "52.164754",
	   Longitude: "4.484354",
	   Url: "https://www.google.com/maps?cid=12838801315663667211",
	}

	place26 := models.ApiPlace {
	   Place: "Thais Restaurant Sisaket",
	   PlaceID: "ChIJyXnzU-nGxUcR2ppD6W1tPug",
	   Latitude: "52.164873",
	   Longitude: "4.484310",
	   Url: "https://www.google.com/maps?cid=16734933584187726554",
	}

	place27 := models.ApiPlace {
	   Place: "Hotspot Central gastrohome",
	   PlaceID: "ChIJq6ozUunGxUcRHYAluDNBRlY",
	   Latitude: "52.164902",
	   Longitude: "4.483833",
	   Url: "https://www.google.com/maps?cid=6216728026020151325",
	}

	place28 := models.ApiPlace {
	   Place: "Red Carpet Lounge",
	   PlaceID: "ChIJUQbpNI7HxUcRq-lc_T4Pco4",
	   Latitude: "52.165330",
	   Longitude: "4.484141",
	   Url: "https://www.google.com/maps?cid=10264283263943895467",
	}

	place29 := models.ApiPlace {
	   Place: "Burger King Stationsplein Leiden",
	   PlaceID: "ChIJG2XiPunGxUcR2aikyGNQsps",
	   Latitude: "52.165287",
	   Longitude: "4.482964",
	   Url: "https://www.google.com/maps?cid=11219118011231545561",
	}

	place30 := models.ApiPlace {
	   Place: "Subway Leiden Station",
	   PlaceID: "ChIJF-k65ejGxUcR9wvNE6444WU",
	   Latitude: "52.166353",
	   Longitude: "4.481180",
	   Url: "https://www.google.com/maps?cid=7341211187898289143",
	}

	place31 := models.ApiPlace {
	   Place: "Wok Your Way",
	   PlaceID: "ChIJTbw65ejGxUcRoSBbFA64-mA",
	   Latitude: "52.166398",
	   Longitude: "4.480989",
	   Url: "https://www.google.com/maps?cid=6988100142429315233",
	}

	place32 := models.ApiPlace {
	   Place: "De Eeuwige Student",
	   PlaceID: "ChIJkzPmq8LGxUcRcltq3bPTnIM",
	   Latitude: "52.170402",
	   Longitude: "4.479155",
	   Url: "https://www.google.com/maps?cid=9483687684802763634",
	}

	place33 := models.ApiPlace {
	   Place: "Zonnestudio SummerTan Leiden",
	   PlaceID: "ChIJ7668qe7GxUcRtRSbrLrHmN4",
	   Latitude: "52.164169",
	   Longitude: "4.484038",
	   Url: "https://www.google.com/maps?cid=16039789677453710517",
	}

	place34 := models.ApiPlace {
		Place: "Johnny's Burger Company",
		PlaceID: "ChIJLUduTJXGxUcRCPCHiPlqpFQ",
		Latitude: "52.168675",
		Longitude: "4.491943",
		Url: "https://www.google.com/maps?cid=6099117415336505352",
	}

	place35 := models.ApiPlace {
		Place: "Onlinekabelshop.nl",
		PlaceID: "ChIJ6ZwfYzvExUcRJeqMWWoy5Gg",
		Latitude: "52.168625",
		Longitude: "4.492267",
		Url: "https://www.google.com/maps?cid=7558221506984864293",
	}

	place36 := models.ApiPlace {
		Place: "Fit For Free Sportschool Leiden Groenoord",
		PlaceID: "ChIJ8-XuaJXGxUcRTTzr7HNdJ5A",
		Latitude: "52.169431",
		Longitude: "4.493442",
		Url: "https://www.google.com/maps?cid=10387373818030799949",
	}

	place37 := models.ApiPlace {
		Place: "Snackbar Hendo Marnixstraat",
		PlaceID: "ChIJpVhgvpbGxUcRIx_Pag0_mNs",
		Latitude: "52.165181",
		Longitude: "4.498983",
		Url: "https://www.google.com/maps?cid=15823466617625321251",
	}

	place38 := models.ApiPlace {
		Place: "Pizzeria Grillroom Flamingo",
		PlaceID: "ChIJ680BX5fGxUcRHt3kbxSJ2jI",
		Latitude: "52.163865",
		Longitude: "4.500852",
		Url: "https://www.google.com/maps?cid=3664391967688940830",
	}

	place39 := models.ApiPlace {
		Place: "Kooipark speelplaats",
		PlaceID: "ChIJu_MfZJnGxUcRaSP3Y-swgpI",
		Latitude: "52.164137",
		Longitude: "4.508473",
		Url: "https://www.google.com/maps?cid=10557054264062452585",
	}

	place40 := models.ApiPlace {
		Place: "New China City",
		PlaceID: "ChIJc61mZpzGxUcRAcMUv33xjag",
		Latitude: "52.161429",
		Longitude: "4.511203",
		Url: "https://www.google.com/maps?cid=12145629292467110657",
	}

	place41 := models.ApiPlace {
		Place: "Milieustraat Gemeente Leiden",
		PlaceID: "ChIJc2e2aJvGxUcR6mS5WNDWaNQ",
		Latitude: "52.159976",
		Longitude: "4.509188",
		Url: "https://www.google.com/maps?cid=15305719523948717290",
	}

	place42 := models.ApiPlace {
		Place: "EscapeRoom Leiden Meelfabriek",
		PlaceID: "ChIJoaOLsprGxUcRUlHsIOxegAc",
		Latitude: "52.159083",
		Longitude: "4.504094",
		Url: "https://www.google.com/maps?cid=540536323542110546",
	}

	place43 := models.ApiPlace {
		Place: "FitnessCentrum L.K.V. de Spartaan",
		PlaceID: "ChIJI2aR9I_GxUcRGDGJ-Qv_NoM",
		Latitude: "52.157358",
		Longitude: "4.502499",
		Url: "https://www.google.com/maps?cid=9455024894607175960",
	}

	place44 := models.ApiPlace {
		Place: "Poolcafé Het Levendaal",
		PlaceID: "ChIJC1noHY_GxUcRvSaJrj7VIQo",
		Latitude: "52.155108",
		Longitude: "4.499335",
		Url: "https://www.google.com/maps?cid=730099079803643581",
	}

	place45 := models.ApiPlace {
		Place: "Plantsoenpark",
		PlaceID: "ChIJX5JRKo_GxUcR4rWzv_En5tQ",
		Latitude: "52.153628",
		Longitude: "4.499166",
		Url: "https://www.google.com/maps?cid=15340993099941000674",
	}

	place46 := models.ApiPlace {
		Place: "Vishandel Atlantic B.V.",
		PlaceID: "ChIJj8Lr647GxUcRELOIQLzrvf4",
		Latitude: "52.155046",
		Longitude: "4.497892",
		Url: "https://www.google.com/maps?cid=18356086850001089296",
	}

	place47 := models.ApiPlace {
		Place: "Wines & More",
		PlaceID: "ChIJFccoYYzGxUcR8b6RZzeaepU",
		Latitude: "52.154583",
		Longitude: "4.493535",
		Url: "https://www.google.com/maps?cid=10771091021561446129",
	}

	place48 := models.ApiPlace {
		Place: "Café van Hout",
		PlaceID: "ChIJ19a3dozGxUcRc9xt4sy9b0A",
		Latitude: "52.153910",
		Longitude: "4.493655",
		Url: "https://www.google.com/maps?cid=4643138428512099443",
	}

	place49 := models.ApiPlace {
		Place: "Trattoria Panini",
		PlaceID: "ChIJfyRPs4zGxUcRkAqZwhSfeQc",
		Latitude: "52.153976",
		Longitude: "4.489436",
		Url: "https://www.google.com/maps?cid=538636541960456848",
	}

	place50 := models.ApiPlace {
		Place: "Siri Thai",
		PlaceID: "ChIJ0559TPPGxUcR8xBJVlOTq-Q",
		Latitude: "52.154747",
		Longitude: "4.489173",
		Url: "https://www.google.com/maps?cid=16477425647804289267",
	}

	place51 := models.ApiPlace {
		Place: "Ristorante Pizzeria Karalis",
		PlaceID: "ChIJr1KhS_PGxUcR7mmG4d9Cjng",
		Latitude: "52.154956",
		Longitude: "4.489073",
		Url: "https://www.google.com/maps?cid=8686954260573088238",
	}

	place52 := models.ApiPlace {
		Place: "Hof van Sijthoff",
		PlaceID: "ChIJA2xyNvPGxUcR0nxI3UoamZE",
		Latitude: "52.155044",
		Longitude: "4.489109",
		Url: "https://www.google.com/maps?cid=10491445715778698450",
	}

	place53 := models.ApiPlace {
		Place: "Eetcafé La Bota",
		PlaceID: "ChIJczHj5_LGxUcRZhuPjtKNVws",
		Latitude: "52.157130",
		Longitude: "4.488023",
		Url: "https://www.google.com/maps?cid=817277792865426278",
	}

	place54 := models.ApiPlace {
		Place: "Het Prentenkabinet",
		PlaceID: "ChIJBa437_LGxUcR0ernxzo2e1w",
		Latitude: "52.157273",
		Longitude: "4.487645",
		Url: "https://www.google.com/maps?cid=6663979699714648785",
	}

	place55 := models.ApiPlace {
		Place: "De FF",
		PlaceID: "ChIJu39e-PLGxUcR2V0ezPjfUUM",
		Latitude: "52.157058",
		Longitude: "4.486588",
		Url: "https://www.google.com/maps?cid=4850904533324160473",
	}

	place56 := models.ApiPlace {
		Place: "Restaurant De Klok",
		PlaceID: "ChIJZ5UzVvLGxUcRB_StE1sSSKU",
		Latitude: "52.157029",
		Longitude: "4.486414",
		Url: "https://www.google.com/maps?cid=11909789396962767879",
	}

	place57 := models.ApiPlace {
		Place: "Café Barrera",
		PlaceID: "ChIJ_x3gVvLGxUcRRuaI_aZ4Ugg",
		Latitude: "52.157106",
		Longitude: "4.486147",
		Url: "https://www.google.com/maps?cid=599674359007209030",
	}

	place58 := models.ApiPlace {
		Place: "Estaminet Schommelen",
		PlaceID: "ChIJs_HUj_LGxUcRYYlUV6SwWiI",
		Latitude: "52.158353",
		Longitude: "4.487513",
		Url: "https://www.google.com/maps?cid=2475485165079791969",
	}

	place59 := models.ApiPlace {
		Place: "Woo Ping",
		PlaceID: "ChIJtZGSmPLGxUcR1FAbBXIsJHA",
		Latitude: "52.158725",
		Longitude: "4.488216",
		Url: "https://www.google.com/maps?cid=8080632499633082580",
	}


	place60 := models.ApiPlace {
		Place: "Aperitivo",
		PlaceID: "ChIJB2IVZftRZUERry75lNTBx2c",
		Latitude: "52.159589",
		Longitude: "4.488077",
		Url: "https://www.google.com/maps?cid=7478158825048583855",
	}

	place61 := models.ApiPlace {
		Place: "Bistro BREE56",
		PlaceID: "ChIJz8Z3Z-3GxUcRvy0k4_y9JQ4",
		Latitude: "52.159641",
		Longitude: "4.488409",
		Url: "https://www.google.com/maps?cid=1019429784509492671",
	}

	place62 := models.ApiPlace {
		Place: "Bistro Malle Jan",
		PlaceID: "ChIJLb8e2_LGxUcRxMtlLRd4exM",
		Latitude: "52.157143",
		Longitude: "4.488600",
		Url: "https://www.google.com/maps?cid=1403847749797268420",
	}

	places = append (
		places, place1, place2, place3, place4, place5, place6, place7, place8, place9, place10, place11, place12,
		place13, place14, place15, place16, place17, place18, place19, place20, place21, place22, place23, place24,
		place25, place26, place27, place28, place29, place30, place31, place32, place33, place34, place35, place36,
		place37, place38, place39, place40, place41, place42, place43, place44, place45, place46, place47, place48,
		place49, place50, place51, place52, place53, place54, place55, place56, place57, place58, place59, place60,
		place61, place62,
	)
}

func (helper PlaceHelper) GetPlaceByName(name string) (*models.ApiPlace, error) {
	for _, place := range places {
		if place.Place == name {
			return &place, nil
		}
	}
	return nil, errors.New("place not found")
}

func (helper PlaceHelper) GetPlaceByID(id string) (*models.ApiPlace, error) {
	for _, place := range places {
		if place.PlaceID == id {
			return &place, nil
		}
	}
	return nil, errors.New("place not found")
}

func (helper PlaceHelper) GetAllPlaces() []models.ApiPlace {
	return places
}