package helpers

import "time"

type TimeHelper struct{}

func (helper TimeHelper) GetTimeFromUnixTimeStamp(timeStamp int) time.Time {
	tm := time.Unix(int64(timeStamp), 0)

	return tm
}

