package helpers

import (
	"errors"
	"iframe/models"
	"sort"
	"strconv"
	"strings"
	"time"
)

type ArrayHelper struct{}

// Hij zegt ik pak een substring tussen bepaalde element posities
// Die element posities zijn bodyString.lastIndexOf(str[0]) + str[0].length en bodyString.lastIndexOf(str[1])
// We moeten de laatste index waar str[0] in voorkomt vinden, en daar dan de str[0] lengte bij optellen
// Daarna moeten we de laatste index in de string van str[1] vinden

func (helper ArrayHelper) FindArrayPositions(busyHourArray string) ([][]int, error){
	var arrayPositionsHolder [][]int
	var arrayPositions []int
	var startArray bool

	if len(busyHourArray) == 0 {
		return nil, errors.New("error in method FindArrayPositions(): busyHourArray is empty")
	}

	for index, arrayStringCharacter := range busyHourArray {
		char := string(arrayStringCharacter)

		if char == "[" {
			arrayPositions = append(arrayPositions, index)
			startArray = true
		}

		if char == "]" {
			if startArray {
				arrayPositions = append(arrayPositions, index + 1)

				// Stops duplicate values, we take the last two array items and append those to arrayPositionsHolder
				lastArrayEntry := len(arrayPositions) - 2
				arrayPositionsHolder = append(arrayPositionsHolder, arrayPositions[lastArrayEntry:])

				startArray = false
			}
		}
	}

	if len(arrayPositionsHolder) == 0 {
		return nil, errors.New("error in method FindArrayPositions(): arrayPositionsHolder is empty")
	}

	return arrayPositionsHolder, nil
}

func (helper ArrayHelper) CreateAndOrderArrayContentsToDays(arrayContents string, arrayPositionsHolder [][]int) (models.Week, error){
	// Google Busy Hours Data starts on Sunday (Weekday(0))
	const startWeekday = time.Weekday(0)

	var currentDay = startWeekday
	var weekDataHolder models.Week
	var currentDayDataHolder models.Day

	if len(arrayContents) == 0 {
		return weekDataHolder, errors.New("error in method CreateAndOrderArrayContentsToDays(): arrayContents is empty")
	}

	if len(arrayPositionsHolder) == 0 {
		return weekDataHolder, errors.New("error in method CreateAndOrderArrayContentsToDays(): arrayPositionsHolder is empty")
	}


	for _, arrayString := range arrayPositionsHolder {

		array := arrayContents[arrayString[0] +1 :arrayString[1] -1]

		splitArray := strings.Split(array, ",")


		//fmt.Println(currentDay)
		//fmt.Println(splitArray)

		if len(splitArray) == 8 {
			currentDay = 0
			currentDayDataHolder.Name = currentDay
			// Append the current day to the week's data holder variable.
			weekDataHolder.Days = append(weekDataHolder.Days, currentDayDataHolder)

			// Reset 'currentDayDataHolder' var to empty.
			currentDayDataHolder = models.Day{}

			continue
		}

		// Array of busy hour data should have 5 of 6 objects in it. If not, continue.
		// Array length is 5 for normal destinations, 6 for stores with a checkout system.
		if len(splitArray) != 5 && len(splitArray) != 6 {
			continue
		}

		// Check if the first array element is of numeric value.
		// This ensures that we don't get useless extra data.
		hour, err := strconv.Atoi(splitArray[0])
		if err != nil {
			continue
		}

		// Check if the second array element is of numeric value.
		// This ensures that we don't get useless extra data.
		busyPercentage, err := strconv.Atoi(splitArray[1])
		if err != nil {
			continue
		}

		hourData := models.DataHour{
			Hour: hour,
			BusyPercentage: busyPercentage,
		}

		currentDayDataHolder.Time = append(currentDayDataHolder.Time, hourData)

		if hour == 23 {
			currentDayDataHolder.Name = currentDay

			// Append the current day to the week's data holder variable.
			weekDataHolder.Days = append(weekDataHolder.Days, currentDayDataHolder)

			// Reset 'currentDayDataHolder' var to empty.
			currentDayDataHolder = models.Day{}

			currentDay ++
		}
	}

	return weekDataHolder, nil
}


func (helper ArrayHelper) OrderArrayWhereCurrentDayIsFirstItem(weekData models.Week) (models.Week, error) {
	var currentDay = time.Now().Weekday()
	var originalDay = time.Now().Weekday()
	var testWeek models.Week

	if len(weekData.Days) == 0 {
		return weekData, errors.New("error in method OrderArrayWhereCurrentDayIsFirstItem(): weekData is empty")
	}

	if len(weekData.Days) == 6 {
		return weekData, errors.New("6 weekdagen ipv 7 wtf")
	}

	for {
		testWeek.Days = append(testWeek.Days, weekData.Days[int(currentDay)])

		if currentDay == 6 {
			currentDay = 0

			if originalDay == 0 {
				break
			}

			continue
		}

		if originalDay != 0 {
			if currentDay == (originalDay - 1) {
				break
			}
		}

		currentDay ++
	}

	return testWeek, nil
}

func (helper ArrayHelper) MergeWeatherDataIntoPlaceStruct(weekData *models.Week, weatherData *models.Weather, selectedDate int) ([]models.CombinedBusyHourWeather, error){
	var timeHelper = new(TimeHelper)
	var weatherHelper = new(WeatherHelper)
	var translationHelper = new(TranslationHelper)

	var holder []models.CombinedBusyHourWeather

	for _, weatherHour := range weatherData.Hourly.Data {
		weatherDataTimeStamp := timeHelper.GetTimeFromUnixTimeStamp(weatherHour.Time)
		timeStampWeekDay := weatherDataTimeStamp.Weekday()
		timeStampHour := weatherDataTimeStamp.Hour()

		for _, weekDay := range weekData.Days {

			if selectedDate == weatherDataTimeStamp.Day() {
				if timeStampWeekDay == weekDay.Name {
					for _, dayHour := range weekDay.Time {
						if dayHour.Hour == timeStampHour {
							combinedBusyHourWeather := models.CombinedBusyHourWeather {
								TimeStamp: weatherDataTimeStamp,
								Hour: weatherDataTimeStamp.Hour(),
								Day: weatherDataTimeStamp.Day(),
								Month: int(weatherDataTimeStamp.Month()),
								Year: weatherDataTimeStamp.Year(),
								Temperature: weatherHour.Temperature,
								BusyPercentage: dayHour.BusyPercentage,
								Summary: translationHelper.TranslateWeatherIcon(weatherHour.Icon),
								WeatherIntensity: weatherHelper.getWeatherIntensity(weatherHour.Icon),
								TotalScore: dayHour.BusyPercentage + weatherHelper.getWeatherIntensity(weatherHour.Icon) - (int(weatherHour.Temperature)),
							}
							if combinedBusyHourWeather.BusyPercentage != 0 {
								holder = append(holder, combinedBusyHourWeather)
							}
						}
					}
				}
			}
		}
	}

	return holder, nil
}

func(helper ArrayHelper) FindBestTimeToVisitLocation(test []models.CombinedBusyHourWeather) []models.CombinedBusyHourWeather{
	sort.SliceStable(test, func(i, j int) bool {
		return test[i].TotalScore < test[j].TotalScore
	})
	return test
}