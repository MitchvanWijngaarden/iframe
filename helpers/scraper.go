package helpers

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"regexp"
	"strings"
)

const BaseGoogleURL string = "https://maps.googleapis.com/maps/api/place/details/json?placeid="

type ScraperHelper struct{}


func (helper ScraperHelper) ScrapeGoogle(c *gin.Context){


	////url := getPlaceUrlByPlaceID("ChIJN1t_tDeuEmsRUsoyG83frY4")
	//url := mockGetPlaceUrlByPlaceID("ChIJN1t_tDeuEmsRUsoyG83frY4")
	//
	//scrape(url)

	fmt.Println("Hey ik ben ik nu ScrapeGoogle.")
}


func (helper ScraperHelper) RemoveUnnecessaryHtml(bodyString string) (string, error){

	const stringStart = "APP_INITIALIZATION_STATE="
	const stringEnd = "window.APP_FLAGS"

	lastIndexStringStart := strings.LastIndex(bodyString, stringStart)
	lengthStringStart := len(stringStart)
	lastIndexStringEnd := strings.LastIndex(bodyString, stringEnd)

	substring := string(bodyString[lastIndexStringStart+lengthStringStart:lastIndexStringEnd])

	if len(substring) == 0 {
		return "", errors.New("error in method RemoveUnnecessaryHtml(): Substring is empty")
	}

	return substring, nil
}



func (helper ScraperHelper) GetSubStringMatch(bodyString string, expression string) (string, error) {
	if len(bodyString) == 0 {
		return "", errors.New("error in method GetSubStringMatch(): Substring is empty")
	}

	if len(expression) == 0 {
		return "", errors.New("error in method GetSubStringMatch(): expression is empty")
	}

	compiledRegex := regexp.MustCompile(expression)
	match := compiledRegex.FindStringSubmatch(bodyString)

	if len(match) == 0 {
		return "", errors.New("error in method GetSubStringMatch(): no match found")
	}

	// Removes useless brackets and gets the less messy string from the regex match.
	filteredResult := match[1][2:]

	if len(filteredResult) == 0 {
		return "", errors.New("error in method GetSubStringMatch(): Potential data change in Google's system")
	}

	return filteredResult, nil
}



//func (ctrl ScraperHelper) GetPlaceUrlByPlaceID(placeID string) (url string){
//	urlToScrape := fmt.Sprintf(BaseGoogleURL+"%vfields=url&key=%v", placeID, "APIKEY")
//
//	apiService := new(services.ApiService)
//
//	body := apiService.GetResponse(urlToScrape)
//
//	// Fill the record with the data from the JSON
//	var placeData models.Place
//
//	// Use json.Decode for reading streams of JSON data
//	if err := json.NewDecoder(body).Decode(&placeData); err != nil {
//		log.Println(err)
//	}
//
//	// Callers should close resp.Body
//	// when done reading from it
//	// Defer the closing of the body
//	defer body.Close()
//
//	placeURL := placeData.Result.URL
//
//	return placeURL
//}


//
//
//
//func busyHours(placeId string) {
//
//
//
//
//}
//
//func scrape(url string) {
//	client := &http.Client{
//		Timeout: 30 * time.Second,
//	}
//
//	// Create and modify HTTP request before sending
//	request, err := http.NewRequest("GET", url, nil)
//	if err != nil {
//		log.Fatal(err)
//	}
//	request.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")
//
//	// Make request
//	response, err := client.Do(request)
//	if err != nil {
//		log.Fatal(err)
//	}
//	defer response.Body.Close()
//
//	body, err := ioutil.ReadAll(response.Body)
//	bodyString := string(body)
//
//
//	scrapeHelper := new(helpers.ArrayHelper)
//
//	substring, err := scrapeHelper.RemoveUnnecessaryHtml(bodyString)
//
//	//substring := string(bodyString[test+test2:test3])
//
//	//fmt.Println(substring)
//
//
//
//	var testRegex = regexp.MustCompile(`\[\[\[7(.*?)\[\[\[`)
//
//
//	x := testRegex.FindStringSubmatch(substring)
//
//	q, err := scrapeHelper.GetSubStringMatch(substring, `\[\[\[7(.*?)\[\[\[`)
//
//	if err != nil {
//		log.Fatal("Error", err)
//	}
//
//
//
//
//	// Create if here if no regex is found, then we must cancel all operations.
//	// if regex == true
//
//	// Get the second le ss messy string from the Regex mutation.
//	y := x[1]
//
//	// Remove useless extra brackets.
//	l := y[2:]
//
//	fmt.Println(q)
//
//
//	//var testArrayPositionsHolder [][]int
//	//
//	//testArrayPositionsHolder = scrapeHelper.FindArrayPositions(q)
//	//
//	//fmt.Println(testArrayPositionsHolder)
//	//
//	//log.Fatal("Cancel")
//
//
//	var values [][]int
//	var arrayPositions []int
//	var startArray bool
//
//	for index, r := range l {
//		c := string(r)
//
//		if c == "[" {
//			arrayPositions = append(arrayPositions, index)
//			startArray = true
//		}
//
//		if c == "]" {
//			// Make sure we're still adding Array Positions of actual Arrays and not just new lines.
//			if startArray {
//				arrayPositions = append(arrayPositions, index + 1)
//
//				x := len(arrayPositions) - 2
//				values = append(values, arrayPositions[x:])
//				startArray = false
//			}
//		}
//	}
//
//
//	weekday := time.Weekday(0)
//
//	var week Week
//	var days int
//	var today Day
//
//	for _, v := range values {
//
//		array := l[v[0] +1 :v[1] -1]
//
//
//		s := strings.Split(array, ",")
//
//		// Array of busy hour data should have 5 of 6 objects in it. If not, continue.
//		// Array length is 5 in normal destinations, 6 in stores with a checkout system.
//		if len(s) != 5 && len(s) != 6 {
//			continue
//		}
//
//		// Check if first two array elements are numeric values.
//		hour, err := strconv.Atoi(s[0])
//		if err != nil {
//			continue
//		}
//
//		busyPercentage, err := strconv.Atoi(s[1])
//		if err != nil {
//			continue
//		}
//
//		hourData := DataHour{
//			Hour: hour,
//			BusyPercentage: busyPercentage,
//		}
//
//		today.Time = append(today.Time, hourData)
//
//
//		if hour == 23 {
//			today.Name = weekday
//			week.Days = append(week.Days, today)
//			weekday ++
//			days ++
//			// Reset 'today' var
//			today = Day{}
//		}
//	}
//
//
//
//
//	kaaaas, err := scrapeHelper.CreateAndOrderArrayContentsToDays(l, values)
//
//
//
//	henk, err := scrapeHelper.OrderArrayWhereCurrentDayIsFirstItem(kaaaas)
//
//	fmt.Println(henk)
//	log.Fatal("cancel")
//
//
//	//weather := new(WeatherController)
//
//	//weatherData := weather.GetWeatherByCity("4.710463", "52.011520")
//
//	//scrapeHelper.MergeWeatherDataIntoPlaceStruct(week, weatherData)
//
//	//appendWeatherDataToPlaceData(week, weatherData)
//}
//
//func getTimeFromUnixTimeStamp(timeStamp int) time.Time {
//	tm := time.Unix(int64(timeStamp), 0)
//
//	return tm
//}
//
//
//
//func appendWeatherDataToPlaceData(weekData Week, weatherData DarkSkyWeather){
//
//	var currentDay = time.Now().Weekday()
//	var originalDay = time.Now().Weekday()
//
//	var testWeek Week
//
//	fmt.Println("Array size van weekData.Days is:", len(weekData.Days))
//
//	//Orders the week array in good positioning
//	for {
//		testWeek.Days = append(testWeek.Days, weekData.Days[int(currentDay)])
//
//		// Als dag == zaterdag, reset currentDay naar 0 zodat we met de nieuwe week kunnen beginnen.
//		if currentDay == 6 {
//			currentDay = 0
//			continue
//		}
//		if currentDay == (originalDay - 1) {
//			break
//		}
//		currentDay ++
//		fmt.Println(currentDay)
//	}
//
//	fmt.Println(testWeek)
//
//	log.Fatal("kaas")
//
//	for _, q := range weatherData.Hourly.Data {
//		weatherDataTimeStamp := getTimeFromUnixTimeStamp(q.Time)
//		timeStampWeekDay := weatherDataTimeStamp.Weekday()
//		timeStampHour := weatherDataTimeStamp.Hour()
//
//		for _, valueKaas := range testWeek.Days {
//			if timeStampWeekDay == valueKaas.Name {
//				for _, testKaaas := range valueKaas.Time {
//					if testKaaas.Hour == timeStampHour {
//						fmt.Println(weatherDataTimeStamp)
//						fmt.Println("\tBusy percentage:\t", testKaaas.BusyPercentage)
//						fmt.Println("\tTemperature:\t\t", q.Temperature, "celcius")
//						fmt.Println("\tWeather:\t\t\t", q.Summary)
//					}
//				}
//			}
//		}
//	}
//}


//out, err := os.Create("BierCafeGouda.txt")
//if err != nil {
//	// panic?
//}
//defer out.Close()
//
//f, err := os.OpenFile("BierCafeGouda.txt", os.O_APPEND|os.O_WRONLY, 0644)
//
//n, err := f.WriteString(bodyString)
//
//f.Close()
//
//fmt.Println(n)