package helpers

import (
	"errors"
	"fmt"
	"iframe/models"
	"log"
	"os"
)

type DatabaseHelper struct {}


var users []*models.User
var fileHelper = new(FileHelper)
var passwordHelper = new(PasswordHelper)
var dbFileName = "db.dat"

func (helper DatabaseHelper) InitDB() {
	if !dataBaseExists() {
		fmt.Println("Database doesn't exist. Creating database db.dat with default admin credentials.")
		password, _ := passwordHelper.HashPassword("admin")
		adminUser := models.User{
			Username: "admin",
			Password: password,
			RecentPlaces: []models.RecentPlace {
				{
					Name: "Kaaaaas",
					PlaceID: "ChIJw4cTe3EUuEcRJMLJxR3Tcwg",
				},

			},
		}
		saveUserToDB(adminUser)
	}

	if err := fileHelper.Load("./" + dbFileName, &users); err != nil {
		log.Fatalln(err)
	}
}

func dataBaseExists() bool{
	if _, err := os.Stat("./" + dbFileName); err == nil {
		return true
	}
	return false
}

func saveDBToLocalFile() {
	if err := fileHelper.Save("./" + dbFileName, &users); err != nil {
		log.Fatalln(err)
	}
}
func saveUserToDB(user models.User) {
	users = append(users, &user)
	saveDBToLocalFile()
}

func getUserFromDatabase(username string) (*models.User, error) {
	for _, user := range users {
		if user.Username == username {
			return user, nil
		}
	}
	return nil, errors.New("user not found")
}

func (helper DatabaseHelper) SaveUserToDatabase(user models.User) {
	saveUserToDB(user)
}

func (helper DatabaseHelper) CheckUserAlreadyExist(username string) bool{
	_, err := getUserFromDatabase(username)
	if err != nil {
		return false
	}

	return true
}

func (helper DatabaseHelper) GetUserFromDatabase(username string) (*models.User, error) {
	return getUserFromDatabase(username)
}

func (helper DatabaseHelper) AddRecentLocationToUser(username string, place models.RecentPlace) {
	user, err := getUserFromDatabase(username)
	if err != nil {
		fmt.Println("Attempting to add recent place to user that doesn't exists.")
		return
	}
	user.RecentPlaces = append(user.RecentPlaces, place)
	fmt.Println(user)
	saveDBToLocalFile()
}


func (helper DatabaseHelper) GetRecentPlacesFromUser(username string) []models.RecentPlace{
	user, err := getUserFromDatabase(username)
	if err != nil {
		fmt.Println("Attempting to get recent place from user that doesn't exists.")
		return nil
	}
	return user.RecentPlaces
}