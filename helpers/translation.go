package helpers

type TranslationHelper struct{}

func (helper TranslationHelper) TranslateWeatherIcon(icon string) string {
	switch icon {
	case "clear-day":
		return "Helder"
	case "clear-night":
		return "Helder (donker)"
	case "partly-cloudy-day":
		return "Plaatselijk bewolkt"
	case "partly-cloudy-night":
		return "Plaatselijk bewolkt (donker)"
	case "cloudy":
		return "Bewolkt"
	case "wind":
		return "Harde windstoten"
	case "fog":
		return "Mistig"
	case "rain":
		return "Regen"
	case "sleet":
		return "Ijzel"
	case "snow":
		return "Sneeuw"
	default:
		return "Error"
	}
}

func (helper TranslationHelper) TranslateWeekDays(day string) string {
	switch day {
	case "Monday":
		return "Maandag"
	case "Tuesday":
		return "Dinsdag"
	case "Wednesday":
		return "Woensdag"
	case "Thursday":
		return "Donderdag"
	case "Friday":
		return "Vrijdag"
	case "Saturday":
		return "Zaterdag"
	case "Sunday":
		return "Zondag"
	default:
		return "Error"
	}
}