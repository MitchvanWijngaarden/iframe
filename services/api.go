package services

import (
	"io"
	"log"
	"net/http"
	"time"
)

type ApiService struct{}

func (service ApiService) GetResponse(url string) (io.ReadCloser, error){
	client := &http.Client{
		Timeout: 30 * time.Second,
	}

	// Create and modify HTTP request before sending
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}

	request.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")

	// Make request
	response, err := client.Do(request)

	return response.Body, err
}




//client := &http.Client{
//		Timeout: 30 * time.Second,
//	}
//
//	// Create and modify HTTP request before sending
//	request, err := http.NewRequest("GET", url, nil)
//	if err != nil {
//		log.Fatal(err)
//	}
//	request.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")
//
//	// Make request
//	response, err := client.Do(request)
//	if err != nil {
//		log.Fatal(err)
//	}
//	defer response.Body.Close()