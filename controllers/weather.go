package controllers

//import (
//	"fmt"
//	"github.com/gin-gonic/gin"
//	"iframe/models"
//	"iframe/services"
//	"io/ioutil"
//	"log"
//	"net/http"
//)
//
//const baseWeatherURL string = "http://api.openweathermap.org/data/2.5/forecast?"
//
//const baseDarkSkyWeatherURL string = "https://api.darksky.net/forecast/c3134975128e0a7ede026b4dabfe75d2/"
//
////WeatherController ...
//type WeatherController struct{}
//
//// Todo: Add busy to every list item using scraper
//type Weather struct {
//	Status  string  `json:"cod"`
//	Message float64 `json:"message"`
//	Cnt     int     `json:"cnt"`
//	List    []struct {
//		Dt   int `json:"dt"`
//		Main struct {
//			Temp      float64 `json:"temp"`
//			TempMin   float64 `json:"temp_min"`
//			TempMax   float64 `json:"temp_max"`
//			Pressure  float64 `json:"pressure"`
//			SeaLevel  float64 `json:"sea_level"`
//			GrndLevel float64 `json:"grnd_level"`
//			Humidity  int     `json:"humidity"`
//			TempKf    float64 `json:"temp_kf"`
//		} `json:"main"`
//		Weather []struct {
//			ID          int    `json:"id"`
//			Main        string `json:"main"`
//			Description string `json:"description"`
//			Icon        string `json:"icon"`
//		} `json:"weather"`
//		Clouds struct {
//			All int `json:"all"`
//		} `json:"clouds"`
//		Wind struct {
//			Speed float64 `json:"speed"`
//			Deg   float64 `json:"deg"`
//		} `json:"wind"`
//		Snow struct {
//			ThreeH float64 `json:"3h"`
//		} `json:"snow"`
//		Sys struct {
//			Pod string `json:"pod"`
//		} `json:"sys"`
//		DtTxt string `json:"dt_txt"`
//		Rain  struct {
//			ThreeH float64 `json:"3h"`
//		} `json:"rain,omitempty"`
//	} `json:"list"`
//	City struct {
//		ID    int    `json:"id"`
//		Name  string `json:"name"`
//		Coord struct {
//			Lat float64 `json:"lat"`
//			Lon float64 `json:"lon"`
//		} `json:"coord"`
//		Country    string `json:"country"`
//		Population int    `json:"population"`
//	} `json:"city"`
//}
//
//type DarkSkyWeather struct {
//	Hourly struct {
//		Data    []struct {
//			Time                int     `json:"time"`
//			Summary             string  `json:"summary"`
//			Icon                string  `json:"icon"`
//			PrecipIntensity     float64 `json:"precipIntensity"`
//			PrecipProbability   float64 `json:"precipProbability"`
//			PrecipType          string  `json:"precipType,omitempty"`
//			Temperature         float64 `json:"temperature"`
//			ApparentTemperature float64 `json:"apparentTemperature"`
//			DewPoint            float64 `json:"dewPoint"`
//			Humidity            float64 `json:"humidity"`
//			Pressure            float64 `json:"pressure"`
//			WindSpeed           float64 `json:"windSpeed"`
//			WindGust            float64 `json:"windGust"`
//			WindBearing         int     `json:"windBearing"`
//			CloudCover          float64 `json:"cloudCover"`
//			UvIndex             int     `json:"uvIndex"`
//			Visibility          float64 `json:"visibility"`
//			Ozone               float64 `json:"ozone"`
//		} `json:"data"`
//	} `json:"hourly"`
//}
//
//func (ctrl WeatherController) AtLocation(c *gin.Context) {
//	//long := c.Param("long")
//	//lat := c.Param("lat")
//
//	//c.JSON(200, GetWeatherByCity(long, lat))
//}
//
//func (ctrl WeatherController) GetWeatherByCity(long string, lat string) (weather DarkSkyWeather) {
//	url := fmt.Sprintf(baseDarkSkyWeatherURL+"%v,%v?extend=hourly&units=si", lat, long)
//
//	//// Build the request
//	//req, err := http.NewRequest("GET", url, nil)
//	//if err != nil {
//	//	log.Fatal("NewRequest: ", err)
//	//	return
//	//}
//	//
//	//// For control over HTTP client headers,
//	//// redirect policy, and other settings,
//	//// create a Client
//	//// A Client is an HTTP client
//	//client := &http.Client{}
//	//
//	//// Send the request via a client
//	//// Do sends an HTTP request and
//	//// returns an HTTP response
//	//resp, err := client.Do(req)
//	//if err != nil {
//	//	log.Fatal("Do: ", err)
//	//	return
//	//}
//	//
//	//// Callers should close resp.Body
//	//// when done reading from it
//	//// Defer the closing of the body
//	//defer resp.Body.Close()
//	//
//	//// Fill the record with the data from the JSON
//	//var weatherData DarkSkyWeather
//	//
//	//// Use json.Decode for reading streams of JSON data
//	//if err := json.NewDecoder(resp.Body).Decode(&weatherData); err != nil {
//	//	log.Println(err)
//	//}
//	//
//	////if weatherData.Status != "200" {
//	////	fmt.Println("Error code: ", weatherData.Status)
//	////	//log.Fatal("Crashed, API error")
//	////}
//
//
//
//
//
//}
