package controllers

import (
	"github.com/gin-gonic/gin"
)

type PlaceController struct{}

func (controller PlaceController) GetAll(c *gin.Context) {
	c.JSON(200, placeHelper.GetAllPlaces())
}

func (controller PlaceController) GetByName(c *gin.Context) {
	placeName := c.Param("name")

	place, err := placeHelper.GetPlaceByName(placeName)
	if err != nil {
		c.JSON(404, nil)
		return
	}
	c.JSON(200, place)
}