package controllers

import (
	"github.com/gin-gonic/gin"
	"iframe/helpers"
	"iframe/models"
	"iframe/services"
	"io/ioutil"
	"strconv"
)

type BusyHourWeatherDataController struct{}

const arrayFilterExpression = `\[\[\[7(.*?)\[\[\[`

var arrayHelper = new(helpers.ArrayHelper)
var placeHelper = new(helpers.PlaceHelper)
var dbHelper = new(helpers.DatabaseHelper)

var selectedDate int

func (ctrl BusyHourWeatherDataController) GetBusyHourWeatherData(c *gin.Context) {
	placeID := c.Param("placeID")
	date, err := strconv.Atoi(c.Param("week"))
	if err != nil {
		c.JSON(500, "Invalid date.")
		return
	}

	selectedDate = date

	busyHourData, busyHourErr := getBusyHourData(placeID)
	if busyHourErr != nil {
		c.JSON(busyHourErr.Status, busyHourErr)
		return
	}

	place, err := placeHelper.GetPlaceByID(placeID)
	long := place.Longitude
	lat := place.Latitude

	if err != nil {
		c.JSON(500, err)
		return
	}

	weatherHelper := new(helpers.WeatherHelper)

	weatherData, weatherDataErr := weatherHelper.GetWeatherByCity(long, lat)
	if weatherDataErr != nil {
		c.JSON(500, weatherDataErr)
		return
	}

	output, outputError := arrayHelper.MergeWeatherDataIntoPlaceStruct(busyHourData, weatherData, selectedDate)

	if weatherDataErr != nil {
		c.JSON(500, outputError)
		return
	}

	testding:= arrayHelper.FindBestTimeToVisitLocation(output)

	c.JSON(200, testding)
}

func getBusyHourData(placeID string) (*models.Week,  *models.ErrorJSON){
	var weekData models.Week

	place, err := placeHelper.GetPlaceByID(placeID)

	placeUrl := place.Url

	if err != nil {
		return nil, &models.ErrorJSON{Status: 404, Message: "No url found for place ID " +placeID}
	}

	apiService := new(services.ApiService)

	googleMapsResponseBody, err := apiService.GetResponse(placeUrl)
	if err != nil {
		return nil, &models.ErrorJSON{Status: 404, Message: "Failed HTTP get to " + placeUrl}
	}

	body, err := ioutil.ReadAll(googleMapsResponseBody)
	if err != nil {
		return nil, &models.ErrorJSON{Status: 500, Message: "Cannot convert body response to string"}
	}

	scrapeHelper := new(helpers.ScraperHelper)

	trimmedBodyString, err := scrapeHelper.RemoveUnnecessaryHtml(string(body))
	if err != nil {
		return nil, &models.ErrorJSON{Status: 404, Message: "Failed to remove unnecessary HTML"}
	}

	busyHoursArray, err := scrapeHelper.GetSubStringMatch(trimmedBodyString, arrayFilterExpression)
	if err != nil {
		return nil, &models.ErrorJSON{Status: 404, Message: "Failed to get Substring Match"}
	}


	arrayPositions, err := arrayHelper.FindArrayPositions(busyHoursArray)
	if err != nil {
		return nil, &models.ErrorJSON{Status: 404, Message: "BusyHoursArray empty or invalid"}
	}

	weekData, err = arrayHelper.CreateAndOrderArrayContentsToDays(busyHoursArray, arrayPositions)
	if err != nil {
		return nil, &models.ErrorJSON{Status: 500, Message: "Can't order weekData array contents to support day/week notation"}
	}

	weekData, err = arrayHelper.OrderArrayWhereCurrentDayIsFirstItem(weekData)
	if err != nil {
		return nil, &models.ErrorJSON{Status: 500, Message: "Can't sort weekData array by weekday"}
	}

	return &weekData, nil
}
