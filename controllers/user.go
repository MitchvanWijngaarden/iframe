package controllers

import (
	"errors"
	"github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"iframe/helpers"
	"iframe/models"
)

type UserController struct{}

var passwordHelper = new(helpers.PasswordHelper)


func (ctrl UserController) GetUserRecentLocations(c *gin.Context) {
	username, err := GetAuthenticatedUserFromContext(c)
	if err != nil {
		c.JSON(403, nil)
		return
	}

	recentPlaces := dbHelper.GetRecentPlacesFromUser(username)

	c.JSON(200, recentPlaces)
}

func (ctrl UserController) CreateNewUser(c *gin.Context) {
	var registerValues models.User

	if err := c.ShouldBind(&registerValues); err != nil {
		c.JSON(400, nil)
		return
	}

	if dbHelper.CheckUserAlreadyExist(registerValues.Username) {
		c.JSON(403, nil)
		return
	}
	registerValues.RecentPlaces = []models.RecentPlace { }

	// BCrypt the password
	password, _ := passwordHelper.HashPassword(registerValues.Password)
	registerValues.Password = password

	dbHelper.SaveUserToDatabase(registerValues)

	c.JSON(200, registerValues)
}


func (ctrl UserController) AddToUserRecentPlaces(c *gin.Context) {
	placeID := c.Param("placeID")

	place, err := placeHelper.GetPlaceByID(placeID)
	if err != nil {
		c.JSON(404, nil)
	}
	username, err := GetAuthenticatedUserFromContext(c)
	if err != nil {
		c.JSON(403, nil)
	}

	location := models.RecentPlace{
		Name: place.Place,
		PlaceID: placeID,
	}

	dbHelper.AddRecentLocationToUser(username, location)

	c.JSON(200, nil)
}

func GetAuthenticatedUserFromContext(c *gin.Context) (string, error) {
	claims := jwt.ExtractClaims(c)
	id, ok := claims["id"]
	if !ok {
		return "", errors.New("failed to extract claim id")
	}
	strID, ok := id.(string)
	if !ok {
		return "", errors.New("failed to parse claim id")
	}
	if strID == "" {
		return "", errors.New("no username recovered")
	}
	// this is their eth address
	return strID, nil
}