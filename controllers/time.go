package controllers

import (
	"github.com/gin-gonic/gin"
	"iframe/helpers"
	"time"
)

type TimeController struct{}

type WeekDing struct{
	Date int
	Day string
}

var translationHelper = new(helpers.TranslationHelper)

func (ctrl TimeController) GetWeekDays(c *gin.Context) {
	var holder []WeekDing
	var currentDay = time.Now()

	for i := 0; i < 7; i ++ {
		day := WeekDing{
			Date: currentDay.Day(),
			Day: translationHelper.TranslateWeekDays(currentDay.Weekday().String()),
		}

		currentDay = currentDay.Add(time.Hour * 24)
		holder = append(holder, day)
	}
	c.JSON(200, holder)
}