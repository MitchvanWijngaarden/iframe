package main

import (
	"github.com/appleboy/gin-jwt"
	cors2 "github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"iframe/controllers"
	"iframe/helpers"
	"iframe/middleware"
	"log"
)

var identityKey = "id"
var dbHelper = new(helpers.DatabaseHelper)
var placeHelper = new(helpers.PlaceHelper)
var passwordHelper = new(helpers.PasswordHelper)
var auth = new(middleware.AuthMiddleware)

func main() {
	apiServer := gin.Default()
	apiServer.Use(cors2.Default())

	dbHelper.InitDB()
	placeHelper.InitPlaces()

	authMiddleware := auth.JwtConfigGenerate()

	apiServer.POST("/login", authMiddleware.LoginHandler)

	apiServer.NoRoute(authMiddleware.MiddlewareFunc(), func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)
		log.Printf("NoRoute claims: %#v\n", claims)
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})

	auth := apiServer.Group("/auth")
	// Refresh time can be longer than token timeout
	auth.GET("/refresh_token", authMiddleware.RefreshHandler)

	v1 := apiServer.Group("/v1")
	{
		/*** places API waypoints***/
		placeController := new(controllers.PlaceController)
		v1.GET("/places/all", placeController.GetAll)
		v1.GET("/places/name/:name", placeController.GetByName)

		/*** week data API waypoints***/
		timeController := new(controllers.TimeController)
		v1.GET("/time/week", timeController.GetWeekDays)

		/*** busyHourWeather API waypoints***/
		busyHourWeatherDataController := new(controllers.BusyHourWeatherDataController)
		v1.GET("/googleplacebusyhoursweatherdata/:placeID/:week", busyHourWeatherDataController.GetBusyHourWeatherData)


		/*** user API waypoints***/
		userController := new(controllers.UserController)
		v1.POST("/users/register", userController.CreateNewUser)

		v1.Use(authMiddleware.MiddlewareFunc())
		{
			v1.GET("/users/places/new/:placeID", userController.AddToUserRecentPlaces)
			v1.GET("/users/places/recent", userController.GetUserRecentLocations)
		}
	}

	apiServer.Run()
}