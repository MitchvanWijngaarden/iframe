package models

import "time"

type CombinedBusyHourWeather struct {
	TimeStamp time.Time
	Hour int
	Day int
	Month int
	Year int
	BusyPercentage 	int
	Temperature float64
	Summary string
	WeatherIntensity int
	TotalScore int
}

