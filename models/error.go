package models

import "fmt"

type ErrorJSON struct {
	Status int
	Message string
}

func (e ErrorJSON) Error() string{
	return fmt.Sprintf("%d - %s", e.Status, e.Message)
}
