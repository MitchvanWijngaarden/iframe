package models

type ApiPlace struct{
	Place string
	PlaceID string
	Latitude string
	Longitude string
	Url string
}
