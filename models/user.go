package models

type User struct {
	Username string `form:"Username" json:"Username" binding:"required"`
	Password string `form:"Password" json:"Password" binding:"required"`
	RecentPlaces []RecentPlace
}

type RecentPlace struct {
	Name string
	PlaceID string
}