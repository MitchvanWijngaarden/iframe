package models

import "time"

type DataHour struct {
	Hour           	int
	BusyPercentage 	int
	TimeWeather 	TimeWeather
}

type TimeWeather struct {
	Temp      float64 	`json:"temp"`
	Humidity  int     	`json:"humidity"`
	WeatherID int		`json:"id"`
}

type Day struct {
	Name time.Weekday
	Time []DataHour
}

type Week struct {
	Days []Day
}